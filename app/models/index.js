const Account = require('@db/account')
const Operation = require('@db/operation')
const Bank = require('@db/bank')

module.exports = {Account,Operation,Bank}