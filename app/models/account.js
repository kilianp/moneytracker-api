const pkg = require('mongoose')
const { model, Schema } = pkg


const Account = model('account', new Schema({
    id: {
      type: String,
      required: true,
      unique: true,
    },

    name: {
      type: String,
      default: '',
    },

    balance: {
      type: Number,
      required: true,
    },

    bank: {
      type: String,
      required: true,
    },

    number: {
      type: Number,
      required: true,
    },
  }))

  module.exports =  Account