const pkg = require('mongoose')
const { model, Schema } = pkg


const Operation = model('operation',new Schema({
  id: {
    type: String,
    required: true,
    unique: true,
  },

  name: {
    type: String,
    default: '',
  },

  amount: {
    type: Number,
    required: true,
  },

  refunded: {
    type: Boolean,
    required: true,
  },

  account: {
    type: String,
    required: true,
  },
  entreprise:{
    type: String,
    required: false,
  },
  date:{
    type: Date,
    required: true,
  },
  arrondi:{
    type: Number,
    required: false,
  },
  logo:{
    type: String,
    required: false,
  }
}));



module.exports = Operation 