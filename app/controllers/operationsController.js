const { Operation, Account } = require('@db')
const mongoose = require('mongoose')
const { updateBalance } = require('./accountsController')

const getOperations = async (req, res) => {
  try {
    const operations = await Operation.find({}).exec()
    console.log(operations)
    res.json(operations)
  } catch (err) {
    res.status(400).json(err)
  }
}

const createOperation = async (req, res) => {
  // ajout d'un élément dans la BDD

  let { name, amount, refunded, account, entreprise, date } = req.body
  const accountSelected = await Account.find({id:account})
  let id = mongoose.Types.ObjectId()
  try {
    let arrondi = 0
    if (accountSelected[0].name.toUpperCase().includes('N26') && amount < 0) {
      arrondi = Math.trunc(Math.abs(amount)) + 1 - Math.abs(amount)
      arrondi = -Math.round(arrondi * 100) / 100
    }

    const operation = await Operation.create({
      id,
      name,
      amount,
      refunded,
      account,
      entreprise,
      date,
      arrondi,
    })
    if (arrondi !=0) amount += arrondi
    if (!refunded) updateBalance(account, amount)
    res.status(201).json(operation)
  } catch (err) {
    console.log(err)
    res.status(400).json(err)
  }
}
const updateLogo = async (req, res) => {
  try {
    console.log(req.body)
    let {operation_id,logo} = req.body;
    const updated = await Operation.updateOne(
      { _id: operation_id },
      { logo: logo },
    )
    console.log(updated)
  } catch (err) {
    throw err
  }
}
module.exports = { getOperations, createOperation,updateLogo }
