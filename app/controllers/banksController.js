const {Bank} = require('@db')

const getBanks = async (req, res) => {
  try {
    const banks = await Bank.find({}).exec()
    res.json(banks)
  } catch (err) {
    console.log(err)
    res.status(400).json(err)
  }
}


module.exports = getBanks