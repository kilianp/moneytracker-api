const { Account, Bank, Operation } = require('@db')
const mongoose = require('mongoose')

const getAccounts = async (req, res) => {
  try {
    const accounts = await Account.find()
    const banks = await Bank.find()
    const operations = await Operation.find()
    console.log(operations)
    let mapped = accounts.map((item) => {
      return {
        id: item._id.toString(),
        number: item.number,
        name: item.name,
        balance: item.balance,
        bank: banks.filter((bank) => bank._id == item.bank)[0],
        operations: operations.filter((op) => op.account == item._id),
      }
    })
    console.log(mapped)
    res.json(mapped)
  } catch (err) {
    res.status(400).json(err)
  }
}

const createAccount = async (req, res) => {
  // ajout d'un élément dans la BDD
  const { name, balance, bank, number } = req.body
  let operations = []
  let id = mongoose.Types.ObjectId()
  try {
    const account = await Account.create({
      id,
      name,
      balance,
      bank,
      number,
      operations,
    })
    res.status(201).json(account)
  } catch (err) {
    console.log(err)
    res.status(400).json(err)
  }
}

const deleteAccount = async (req, res) => {
  try {
    const deleted = await Account.deleteOne({ _id: req.params.id })
    res.status(200).json(deleted)
  } catch (err) {
    console.log(err)
    res.status(400).json(err)
  }
}

const updateBalance = async (account_id, amount) => {
  try {
    const account = await Account.find({ _id: account_id })
    let balance = parseFloat(account[0].balance)
    amount = parseFloat(amount)
    let total = balance + amount
    const updated = await Account.updateOne(
      { _id: account_id },
      { balance: total },
    )
  } catch (err) {
    throw err
  }
}

module.exports = { getAccounts, createAccount, deleteAccount, updateBalance }
