const _ = require('lodash')
const express = require('express')
const helmet = require('helmet') // Enforces good security practices
const cors = require('cors') // CORS support for API
const app = express()
const pkg = require('mongoose')
const { connect, model, Schema } = pkg
var bodyParser = require('body-parser')
const router = require('./routes/index')
// Middlewares
app.set('port', process.env.PORT || 3000)
// 406 & 415 status code middleware
app.use(cors());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  if ('OPTIONS' == req.method) {
     res.sendStatus(200);
   }
   else {
     next();
   }});


connect(
  'mongodb+srv://moneytracker:moneytracker@cluster0.llp9m.mongodb.net/moneytracker',
)
  .then(() => {
    app.use('/api/', router)
    /** 
    app.get('/accounts/full', 
    })

    app.get('/accounts/:id', async (req, res) => {
      const { id } = req.params

      try {
        const account = await Account.findById(id).exec()

        if (account) {
          res.json(account)
        } else {
          res.status(404).json({
            code: 404,
            message: 'Not Found',
          })
        }
      } catch (err) {
        res.status(400).json(err)
      }
    })


    app.put('/accounts/:id', async (req, res) => {
      const { title, description, year, director, producers } = req.body
      const { id } = req.params

      try {
        const account = await Account.findByIdAndUpdate(
          id,
          { title, description, year, director, producers },
          { runValidators: true },
        )

        if (account) {
          res.status(204).end()
        } else {
          res.status(404).json({
            code: 404,
            message: 'Not Found',
          })
        }
      } catch (err) {
        
        res.status(400).json(err)
      }
    })

    app.delete('/accounts/:id', async (req, res) => {
      const { id } = req.params

      try {
        const removed = await Account.findByIdAndRemove(id)

        if (removed) {
          res.status(204).end()
        } else {
          res.status(404).json({
            code: 404,
            message: 'Not Found',
          })
        }
      } catch (err) {
        
        res.status(400).json(err)
      }
    })
*/
  })
  .catch((err) => {
    console.log(err)
    console.log('Mongoose connection error')
  })

module.exports = app
