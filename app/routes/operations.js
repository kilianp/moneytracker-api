const { Router } = require('express')
const {
  getOperations,
  createOperation,
  updateLogo,
} = require('@controllers/operationsController')
const OperationRouter = Router()

OperationRouter.get('/', getOperations)
OperationRouter.post('/', createOperation)
OperationRouter.put('/logo', updateLogo)
module.exports = OperationRouter
