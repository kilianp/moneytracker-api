const { Router } =  require('express')
const getBanks = require('@controllers/banksController')
const BankRouter = Router()

BankRouter.get('/',getBanks)

module.exports = BankRouter
