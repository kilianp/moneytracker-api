const { Router } =  require('express')
const {getAccounts,createAccount,deleteAccount} = require('../controllers/accountsController.js')
const AccountRouter = Router()

AccountRouter.get('/',getAccounts)
AccountRouter.post('/',createAccount)
AccountRouter.delete('/:id',deleteAccount)
module.exports = AccountRouter