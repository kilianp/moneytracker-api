const { Router } = require('express')
const router = Router()

const AccounRouter = require('./accounts')
const BankRouter = require('./bank')
const OperationRouter = require('./operations')

router.use('/accounts',AccounRouter),
router.use('/operations',OperationRouter)
router.use('/banks',BankRouter)

module.exports = router